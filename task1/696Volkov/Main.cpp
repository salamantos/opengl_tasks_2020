#include "Main.h"

#define GLM_ENABLE_EXPERIMENTAL

#include <glm/gtx/rotate_vector.hpp>

#undef GLM_ENABLE_EXPERIMENTAL

static constexpr float kTreeSegmentHeightRatio = 0.7f;
static constexpr float kTreeSegmentWidthRatio = 0.6f;
static constexpr float randomFactor = 0.0f;
static constexpr size_t kTreeTrunkBottomVertices = 128;
static constexpr size_t kTreeDepth = 8;

TreeGrower::TreeGrower(glm::vec3 trunkStart, glm::vec3 trunkEnd, float r) {
    generateSegment(trunkStart, trunkEnd, r, kTreeTrunkBottomVertices, 0);
    srand(42);
    m_bottomVertices.clear();
    m_bottomNormals.clear();
    m_topVertices.clear();
    m_topNormals.clear();
}

MeshPtr TreeGrower::getMesh() {
    DataBufferPtr vertexBuffer = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    vertexBuffer->setData(m_vertices.size() * sizeof(float) * 3, m_vertices.data());

    DataBufferPtr normalBuffer = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    normalBuffer->setData(m_normals.size() * sizeof(float) * 3, m_normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, vertexBuffer);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, normalBuffer);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(static_cast<GLuint>(m_vertices.size()));

    return mesh;
}

float TreeGrower::randomize(float value) {
    return value + (rand() % 21 - 10) * randomFactor;
}

void TreeGrower::generateSegmentPoints(glm::vec3 bottomPos, glm::vec3 topPos, float bottomR, int bottomVerticesCount) {
    m_bottomVertices.clear();
    m_bottomNormals.clear();
    m_topVertices.clear();
    m_topNormals.clear();

    auto topR = bottomR * kTreeSegmentWidthRatio;
    auto topVerticesCount = bottomVerticesCount / 2;

    auto rotationMatrix = glm::orientation(glm::normalize(topPos - bottomPos), glm::vec3(0.0f, 0.0f, 1.0f));

    for (auto i = 0; i < bottomVerticesCount; ++i) {
        float angle = 2.0f * glm::pi<float>() * i / bottomVerticesCount;
        glm::vec4 pt(cosf(angle), sinf(angle), 0.0f, 0.0f);

        m_bottomVertices.emplace_back(glm::vec3(rotationMatrix * (pt * bottomR)) + bottomPos);
        m_bottomNormals.emplace_back(rotationMatrix * pt);

        if (i % 2 == 0) {
            glm::vec4 topPt = pt * topR + glm::vec4(0.0f, 0.0f, glm::distance(bottomPos, topPos), 0.0f);
            m_topVertices.emplace_back(glm::vec3(rotationMatrix * topPt) + bottomPos);
            m_topNormals.emplace_back(rotationMatrix * pt);
        }
    }
}

void TreeGrower::generateBottomTriangle(size_t bottomIdx1, size_t bottomIdx2, size_t topIdx) {
    size_t bottomVerticesCount = m_bottomVertices.size();
    size_t topVerticesCount = m_topVertices.size();

    m_vertices.push_back(m_bottomVertices[bottomIdx1 % bottomVerticesCount]);
    m_vertices.push_back(m_bottomVertices[bottomIdx2 % bottomVerticesCount]);
    m_vertices.push_back(m_topVertices[topIdx % topVerticesCount]);
    m_normals.push_back(m_bottomNormals[bottomIdx1 % bottomVerticesCount]);
    m_normals.push_back(m_bottomNormals[bottomIdx2 % bottomVerticesCount]);
    m_normals.push_back(m_topNormals[topIdx % topVerticesCount]);
}

void TreeGrower::generateTopTriangle(size_t bottomIdx, size_t topIdx1, size_t topIdx2) {
    size_t bottomVerticesCount = m_bottomVertices.size();
    size_t topVerticesCount = m_topVertices.size();

    m_vertices.push_back(m_bottomVertices[bottomIdx % bottomVerticesCount]);
    m_vertices.push_back(m_topVertices[topIdx1 % topVerticesCount]);
    m_vertices.push_back(m_topVertices[topIdx2 % topVerticesCount]);
    m_normals.push_back(m_bottomNormals[bottomIdx % bottomVerticesCount]);
    m_normals.push_back(m_topNormals[topIdx1 % topVerticesCount]);
    m_normals.push_back(m_topNormals[topIdx2 % topVerticesCount]);
}

void
TreeGrower::generateSegment(glm::vec3 bottomPos, glm::vec3 topPos, float bottomR, int bottomVerticesCount, int depth) {
    generateSegmentPoints(bottomPos, topPos, bottomR, bottomVerticesCount);

    for (size_t i = 0; i * 2 < bottomVerticesCount; ++i) {
        generateBottomTriangle(2 * i + 1, 2 * i + 2, i + 1);
        generateTopTriangle(2 * i + 1, i + 1, i);
        generateBottomTriangle(2 * i + 2, 2 * i + 3, i + 1);
    }

    generateNextSegments(bottomPos, topPos, bottomR, bottomVerticesCount, depth);
}

void TreeGrower::generateNextSegments(glm::vec3 bottomPos, glm::vec3 topPos, float bottomR, int bottomVerticesCount,
                                      int depth) {
    if (depth >= kTreeDepth) {
        return;
    }

    auto rotationMatrix = glm::orientation(glm::normalize(topPos - bottomPos), glm::vec3(0.0f, 0.0f, 1.0f));

    std::vector<Branch> branches;
    if (depth == 0) {
        branches.emplace_back(0.55f / 0.4f, 0.0f, -1.5f, 0.0f, 0.0f);
        branches.emplace_back(0.7f / 0.4f, 0.0f, 0.7f, 0.0f, 0.0f);

//        branches.emplace_back(0.6f / 0.5f, 0.3f, -1.5f, 0.0f, 0.0f);
//        branches.emplace_back(0.7f / 0.5f, 0.7f, 0.7f, 0.0f, 0.0f);
//        branches.emplace_back(0.4f / 0.5f, -2.5f, 0.0f, 0.0f, 0.0f);
    } else {
        branches.emplace_back(0.6f, 0.1f, -1.7f, 0.0f, 0.0f);
        branches.emplace_back(0.7f, -0.1f, 0.7f, 0.0f, 0.0f);
        branches.emplace_back(0.5f, 0.6f, -0.1f, 0.0f, 0.0f);
        branches.emplace_back(0.5f, -0.6f, -0.1f, 0.0f, 0.0f);
    }

    auto segmentLength = glm::distance(topPos, bottomPos);

    for (auto&& branch : branches) {
//        float angle = branch.angle;
        float lengthRatio = randomize(branch.lengthRatio);
        auto& directionProjection = branch.directionProjection;
        auto direction = glm::normalize(
                topPos - bottomPos + glm::vec3(rotationMatrix * directionProjection) * segmentLength);
        auto nextTopPos = topPos + direction * segmentLength * lengthRatio;
        int currentBottomVerticesCount = bottomVerticesCount;
        if (depth >= 5) {
            currentBottomVerticesCount = bottomVerticesCount / 2;
        }

        generateSegment(topPos, nextTopPos, bottomR * kTreeSegmentWidthRatio, currentBottomVerticesCount, depth + 1);
    }
}

int main() {
    TreeApplication app;
    app.start();

    return 0;
}