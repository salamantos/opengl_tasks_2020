#include "Main.h"

void TreeApplication::makeScene() {
    Application::makeScene();

    getCameraMover() = std::make_shared<FreeCameraMover>();

    m_tree = TreeGrower(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.4f), 0.06f).getMesh();
    m_tree->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));

    //Создаем шейдерную программу
    m_shader = std::make_shared<ShaderProgram>("696VolkovData1/shaderNormal.vert", "696VolkovData1/shader.frag");
}

void TreeApplication::draw() {
    Application::draw();

    int width, height;
    glfwGetFramebufferSize(getWindow(), &width, &height);

    glViewport(0, 0, width, height);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Устанавливаем шейдер.
    m_shader->use();

    //Устанавливаем общие юниформ-переменные
    m_shader->setMat4Uniform("viewMatrix", getCamera().viewMatrix);
    m_shader->setMat4Uniform("projectionMatrix", getCamera().projMatrix);

    //Рисуем первый меш
    m_shader->setMat4Uniform("modelMatrix", m_tree->modelMatrix());
    m_tree->draw();
}
